#!/usr/bin/env python3

import logging
import sqlite3 as sqlite

from chatterbot import ChatBot


custom_logger = logging.getLogger(__name__)


def gitter_chatbot():
    return ChatBot(
        'botlechat',
        input_adapter="chatterbot.input.Gitter",
        output_adapter="chatterbot.output.Gitter",
        gitter_api_token="319c4050eed21477d80d93866102e9293ceedc01", # lechien
        #gitter_api_token="a5135fd8a8db4fe6c1d3b09ffcc3ab4d492341b7", # lechat
        gitter_room="botlechat/botlechat",
        gitter_only_respond_to_mentions=False,
        #trainer='chatterbot.trainers.ChatterBotCorpusTrainer',
        #database='/home/nicolas/project/lechien/database.sqlite',
        trainer='chatterbot.trainers.ListTrainer',
        logger=custom_logger,
    )


def terminal_chatbot():
    return ChatBot(
        'botlechat',
        input_adapter="chatterbot.input.TerminalAdapter",
        output_adapter="chatterbot.output.TerminalAdapter",
        #storage_adapter="chatterbot.storage.SQLStorageAdapter",
        logic_adapters=[
            {
                "import_path": "chatterbot.logic.BestMatch",
                "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
                "response_selection_method": "chatterbot.response_selection.get_first_response",
            },
        ],
        trainer='chatterbot.trainers.ListTrainer',
        #database='/home/nicolas/project/lechien/database.sqlite',
        logger=custom_logger,
    )



###
#
# chatbot instance
#
###

#chatbot = gitter_chatbot()
chatbot = terminal_chatbot()

#chatbot.output.send_message('wouaf wouaf')

for x in ['salut', 'slt', 'bonjour', 'bjr', 'hi', 'hey', 'hello']:
    chatbot.train([
        x,
        "bonjour, comment t'appelles-tu ?",
    ])

for x in ["devops", 'vuejs', 'table', 'money', 'caramba', 'manger', 'x86',
        'mac', 'windows']:
    chatbot.train([x, "Désolé, je n'ai pas compris."])

for x in ['c#', 'c', 'sql', 'html', 'ruby', 'dos']:
    chatbot.train([
        x,
        "Oh, j'aime bien également. Quel est ton autre langage préféré ?",
    ])

for x in ['java', 'python']:
    chatbot.train([
        x,
        "start_test_oop('{}')".format(x),
    ])

for x in [ "bye", "au revoir", "++", "@+", "à +", "ciao"]:
    chatbot.train([
        x,
        "Au revoir !",
    ])

## Train based on the english corpus
#chatbot.train("chatterbot.corpus.english")

## Get a response to an input statement
#chatbot.get_response("Hello, how are you today?")


def expect_yes(text):
    return text.lower() in ['oui', 'yes', 'o', 'y', 'évidemment']

def expect_no(text):
    return text.lower() in ['non', 'no', 'n']

print("type now")
name = None
email = None
while True:
    try:
        def start_test_oop(lang):
            if lang not in ['java', 'python']:
                return

            note = 0
            max_note = 0
            # Q1
            max_note += 1
            chatbot.output.send_message("Est-ce que {} est un langage orienté objet ?".format(lang))
            statement, resp, _ = chatbot.get_response(None)
            if expect_yes(statement.text):
                note += 1
            # Q2
            max_note += 1
            chatbot.output.send_message("Avec {}, doit-on gérer la mémoire ?".format(lang))
            statement, resp, _ = chatbot.get_response(None)
            if expect_no(statement.text):
                note += 1
            # Note
            chatbot.output.send_message("Ta note sur {} est {}/{}".format(
                lang, str(note), str(max_note)))

            chatbot.output.send_message("Si tu connais un autre langage, n'hésite pas, sinon merci et au revoir !")
            return note


        statement, resp, _ = chatbot.get_response(None)
        if resp.text.startswith("start_test_oop("):
            eval(resp.text)
        #resp = chatbot.get_response(None) # Ok
        else:
            chatbot.output.process_response(resp)

        if resp.text == "bonjour, comment t'appelles-tu ?":
            statement, resp, _ = chatbot.get_response(None)
            name = statement.text
            chatbot.output.send_message("bonjour {}, quel est ton email ?".format(name))

            statement, resp, _ = chatbot.get_response(None)
            #print(statement)
            #print(statement.text)

            # if not '@' in statement.text:
                # chatbot.output.send_message("Désolé, je n'ai pas compris ton email. Au revoir !")
            # else:
                # email = statement.text
                # chatbot.output.send_message("Merci, j'ai noté que ton email est : {}".format(email))
            chatbot.output.send_message("Quelle est ton langage favori ?")

    except (KeyboardInterrupt, EOFError, SystemExit):
        break
