#!/usr/bin/env python3

from chatterbot import ChatBot

chatbot = ChatBot(
    'botlechat',
    input_adapter="chatterbot.input.Gitter",
    output_adapter="chatterbot.output.Gitter",
    gitter_api_token="a5135fd8a8db4fe6c1d3b09ffcc3ab4d492341b7",
    gitter_room="botlechat/botlechat",
    gitter_only_respond_to_mentions=False,
    trainer='chatterbot.trainers.ChatterBotCorpusTrainer',
)

## Train based on the english corpus
#chatbot.train("chatterbot.corpus.english")

## Get a response to an input statement
#chatbot.get_response("Hello, how are you today?")

if __name__ == '__main__':
    chatbot.output.send_message('miaou')
    while True:
        try:
            response = chatbot.get_response(None)
            print(response)
        except (KeyboardInterrupt, EOFError, SystemExit):
            print('stopping now')
            break

    print('Bye')
